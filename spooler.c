/* 
 * File:   main.c
 * Author: Charles Gathuru
 * Student no: 260457189
 *
 * Created on February 14, 2014, 1:17 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <time.h>

pthread_mutex_t mutex;
pthread_cond_t condc, condp;
int head=0, tail = -1;
int *buffer;
int buffersize;

bool isEmpty()
{
    if(tail == -1 && head==0)
        return true;
    return false;
}

bool isFull()
{
    if(!isEmpty()){
        if((tail+1) % buffersize == head ) 
            return true;
     }
     return false;
}

void* client(void* ptr)
{
    int i;
    
    
    while(true)
    {
        pthread_mutex_lock(&mutex); //protect the buffer
        int id = (int*) ptr; //get the thread id
        
        int numpages = rand() %11 +1; //generate and random number between 1 and 11
        while(isFull())
        {
            pthread_cond_wait(&condc, &mutex); //sleep until empty
            printf("Client %i wakes up, puts request in Buffer[%i]\n",id,(tail+ 1) % buffersize);
        }
        
        //
        numpages = rand() %11 +1; 
        tail= (tail+ 1) % buffersize; //increment tail with wrap around
        buffer[tail] = numpages; //put pages to print in the buffer
        printf("Client %i has %i pages to print, puts request in Buffer[%i]\n",id,numpages,tail);
        
      
        pthread_cond_signal(&condp); //wake up printer
        pthread_mutex_unlock(&mutex); //unlock
    }
    
    
    pthread_exit(0);
    
}

void* printer(void* ptr)
{
    int i, contents;
    while(true)
    {
        pthread_mutex_lock(&mutex); //protect the buffer
        int id = (int*) ptr; //get the thread id

        while(isEmpty())
        {
            printf("No request in buffer, Printer %i sleeps\n",id);
            pthread_cond_wait(&condp, &mutex); //sleep until there is something the buffer
        }          

        /* Execute Critical section */
        contents = buffer[head]; //get the contents from the buffer
        int oldhead = head;
        if (head == tail) {
            head = 0;
            tail = -1;
        }
        else head = (head +1) % buffersize; //increment head with wraparound
        ///
        printf("Printer %i starts printing %i pages from Buffer[%i]\n",id,contents,oldhead);
        sleep(contents); //simulate printing
        printf("Printer %i finishes printing %i pages from Buffer[%i]\n",id,contents,oldhead);
        ///
        pthread_cond_signal(&condc); //wake up client
        pthread_mutex_unlock(&mutex); //unlock
    }
    
}
/*
 * 
 */
int main(int argc, char* argv[]) 
{
    pthread_mutex_init(&mutex,NULL); //Initialize the mutex
    pthread_cond_init(&condc, NULL); // Initialize the consumer conditional variable
    pthread_cond_init(&condp,NULL); // Initialize the producer conditional variable
    
    /*Get the command line arguments*/
    int threadsc = atoi(argv[1]);
    int threadsp = atoi(argv[2]);
    buffersize = atoi(argv[3]);
    
    /* Initialize create space for c clients, p printers and b buffer size*/
    pthread_t * threadc = malloc(sizeof(pthread_t)*threadsc);
    pthread_t * threadp = malloc(sizeof(pthread_t)*threadsp);
    buffer = malloc (buffersize * sizeof (buffer[0]));
    printf("%i Clients, %i Printers, Buffer size: %i \n  ",threadsc,threadsp,buffersize);
    
    /* Create the client threads */
    int i;
    for(i = 0; i< threadsc; i++)
    {
        pthread_create(&threadc[i], NULL, client, (void*)i);
    }
    /* Create printer threads */
    for(i = 0; i< threadsp; i++)
    {
        pthread_create(&threadp[i], NULL, printer,(void*)i);
    }
    
    /* Wait for threads to finish */
    for(i = 0; i< threadsc; i++)
    {
        pthread_join(threadc[i],NULL);
    }
    for(i = 0; i< threadsp; i++)
    {
        pthread_join(threadp[i],NULL);
    }
    
    /* Clean up */
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&condp);
    pthread_cond_destroy(&condc);
    return (EXIT_SUCCESS);
}
